#!/bin/bash

docker tag embedash:1.1-SNAPSHOT 644435390668.dkr.ecr.ap-south-1.amazonaws.com/mike-ted:latest

docker tag ted_nginx:1.1-SNAPSHOT 644435390668.dkr.ecr.ap-south-1.amazonaws.com/minney-nginx:latest

docker push 644435390668.dkr.ecr.ap-south-1.amazonaws.com/mike-ted:latest

docker push 644435390668.dkr.ecr.ap-south-1.amazonaws.com/minney-nginx:latest
